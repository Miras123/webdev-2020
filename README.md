# Индивидуальные задания

**2) Сайт-визитка (Google Sites) -** https://gitlab.com/Miras123/webdev-2020/-/blob/master/02/README.md


**4) Сайт-визитка на разных конструкторах-сайтов -** https://gitlab.com/Miras123/webdev-2020/-/blob/master/04/readme.md


**6) Сайт-визитка WordPress (local, openserver) -** https://gitlab.com/Miras123/webdev-2020/-/blob/master/06/README.md


**7) Сайт-визитка на Wordpress Gutenberg/Wordpress Elementor и на сайте WordPress.com -** https://gitlab.com/Miras123/webdev-2020/-/blob/master/07/README.md


**8) Мой GLOSSARY -** https://gitlab.com/Miras123/webdev-2020/-/blob/master/08/GLOSSARY.md


**90) КР -** https://gitlab.com/Miras123/webdev-2020/-/blob/master/90/README.md

**"Сертификат ИНТУИТ по Linux"**

![](intuit1F.jpg)

**"Сертификат ИНТУИТ по PHP 5.2"**

![](intuit2F.jpg)

